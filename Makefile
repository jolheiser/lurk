.PHONY: build
build:
	go build

.PHONY: fmt
fmt:
	go fmt ./...

.PHONY: test
test:
	go test -race ./...

.PHONY: vet
vet:
	go vet ./...