package main

import (
	"os"
	"regexp"
	"testing"
)

func TestMain(m *testing.M) {
	os.Exit(m.Run())
}

func TestMatchesAny(t *testing.T) {
	tt := []struct {
		Regex string
		Input string
		Match bool
	}{
		{Regex: `JAVA`, Input: "java", Match: false},
		{Regex: `(?i)JAVA`, Input: "java", Match: true},
		{Regex: `white`, Input: "whitelist", Match: true},
		{Regex: `\bwhite\b`, Input: "whitelist", Match: false},
		{Regex: `\bwhite\b`, Input: "white list", Match: true},
		{Regex: `\bwhite\b`, Input: "a white list", Match: true},
		{Regex: `\swhite\s`, Input: "whitelist", Match: false},
		{Regex: `\swhite\s`, Input: "white list", Match: false},
		{Regex: `\swhite\s`, Input: "a white list", Match: true},
	}

	for _, tc := range tt {
		t.Run(tc.Regex, func(t *testing.T) {
			r := regexp.MustCompile(tc.Regex)
			if r.MatchString(tc.Input) != tc.Match {
				t.Fail()
			}
		})
	}
}
