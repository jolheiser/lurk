package main

import (
	"flag"
	"os"
	"os/signal"
	"syscall"

	"go.jolheiser.com/lurk/config"
	"go.jolheiser.com/lurk/handler"

	"github.com/dghubble/go-twitter/twitter"
	"github.com/dghubble/oauth1"
	"github.com/turnage/graw"
	"github.com/turnage/graw/reddit"
	"go.jolheiser.com/beaver"
)

var (
	configPath string
	debug      bool
)

func main() {
	flag.StringVar(&configPath, "config", "lurk.toml", "Path to lurk's config file")
	flag.BoolVar(&debug, "debug", false, "Turn on debug mode")
	flag.Parse()

	beaver.Console.Format = beaver.FormatOptions{
		TimePrefix:  true,
		StackPrefix: true,
		StackLimit:  15,
		LevelPrefix: true,
		LevelColor:  true,
	}
	if debug {
		beaver.Console.Level = beaver.DEBUG
	}

	cfg, err := config.Load(configPath)
	if err != nil {
		beaver.Fatal(err)
	}

	// Reddit
	go lurkReddit(cfg)

	// Twitter
	go lurkTwitter(cfg)

	beaver.Info("Lurk is ready to start lurking!")
	ch := make(chan os.Signal)
	signal.Notify(ch, syscall.SIGINT, syscall.SIGTERM)
	<-ch
}

func lurkReddit(cfg *config.Config) {
	bot, err := reddit.NewBot(reddit.BotConfig{
		Agent: cfg.Reddit.UserAgent(),
		App: reddit.App{
			ID:       cfg.Reddit.ClientID,
			Secret:   cfg.Reddit.ClientSecret,
			Username: cfg.Reddit.Username,
			Password: cfg.Reddit.Password,
		},
	})
	if err != nil {
		beaver.Fatal(err)
	}

	_, wait, err := graw.Run(&handler.Reddit{
		Config: cfg,
	}, bot, graw.Config{
		Subreddits: cfg.Reddit.SubRedditNames(),
	})
	if err != nil {
		beaver.Errorf("could not run reddit bot: %v", err)
		return
	}
	if err := wait(); err != nil {
		beaver.Fatal(err)
	}
}

func lurkTwitter(cfg *config.Config) {
	twitterConfig := oauth1.NewConfig(cfg.Twitter.ConsumerKey, cfg.Twitter.ConsumerSecret)
	token := oauth1.NewToken(cfg.Twitter.AccessToken, cfg.Twitter.AccessSecret)

	httpClient := twitterConfig.Client(oauth1.NoContext, token)
	client := twitter.NewClient(httpClient)

	// Just to test if we have valid auth
	_, _, err := client.Timelines.HomeTimeline(&twitter.HomeTimelineParams{
		Count: 1,
	})
	if err != nil {
		beaver.Fatal(err)
	}

	for _, filter := range cfg.Twitter.Filters {
		stream, err := client.Streams.Filter(&twitter.StreamFilterParams{
			Follow:        filter.Follows,
			Locations:     filter.Locations,
			StallWarnings: twitter.Bool(false),
			Track:         filter.Tracks,
		})
		if err != nil {
			beaver.Fatal(err)
		}

		lurker := &handler.Twitter{
			Filter: filter,
			Stream: stream,
		}

		go lurker.Run()
	}
}
