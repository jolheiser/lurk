# lurk

A lurker used to notify a Discord channel via webhook 
whenever a matching submission is made.

See the [example config](lurk.sample.toml).