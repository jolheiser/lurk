module go.jolheiser.com/lurk

go 1.16

require (
	github.com/dghubble/go-twitter v0.0.0-20200725221434-4bc8ad7ad1b4
	github.com/dghubble/oauth1 v0.6.0
	github.com/pelletier/go-toml v1.8.1
	github.com/turnage/graw v0.0.0-20200404033202-65715eea1cd0
	go.jolheiser.com/beaver v1.0.2
	go.jolheiser.com/disco v0.0.3
)
