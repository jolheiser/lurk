package handler

import (
	"context"
	"fmt"

	"go.jolheiser.com/lurk/config"

	"github.com/dghubble/go-twitter/twitter"
	"go.jolheiser.com/beaver"
	"go.jolheiser.com/disco"
)

type Twitter struct {
	Filter config.Filter
	Stream *twitter.Stream
}

func (t *Twitter) Run() {
	beaver.Debugf("setting up stream for %v", t.Filter)
	demux := twitter.NewSwitchDemux()
	demux.Tweet = t.Tweet

	beaver.Debugf("streaming %v", t.Filter)
	demux.HandleChan(t.Stream.Messages)
	beaver.Debugf("disconnected from stream: %v", t.Filter)
}

func (t *Twitter) Tweet(tweet *twitter.Tweet) {
	beaver.Debugf("new tweet for %v", t.Filter)

	if t.Filter.FollowStrict {
		if tweet.InReplyToStatusIDStr != "" {
			beaver.Debug("tweet is a reply")
			return
		}
		if tweet.RetweetedStatus != nil {
			beaver.Debug("tweet is a retweet")
			return
		}
		var match bool
		for _, id := range t.Filter.Follows {
			if id == tweet.User.IDStr {
				match = true
				break
			}
		}
		if !match {
			beaver.Debug("tweet did not match any follow IDs")
			return
		}
	}

	w := &disco.Webhook{
		Username:  "@" + tweet.User.ScreenName,
		AvatarURL: tweet.User.ProfileImageURLHttps,
		Content:   fmt.Sprintf("https://twitter.com/%d/status/%d", tweet.User.ID, tweet.ID),
	}
	if _, err := w.Send(context.Background(), t.Filter.Webhook); err != nil {
		beaver.Error(err)
	}
}
