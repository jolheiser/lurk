package config

import (
	"fmt"
	"regexp"
)

type RedditConfig struct {
	SubReddits []*SubReddit          `toml:"sub"`
	Map        map[string]*SubReddit `toml:"-"`

	// Agent file
	AppName string `toml:"app_name"`
	Version string `toml:"version"`

	ClientID     string `toml:"client_id"`
	ClientSecret string `toml:"client_secret"`

	Username string `toml:"username"`
	Password string `toml:"password"`
}

type SubReddit struct {
	Name             string           `toml:"name"`
	IconURL          string           `toml:"icon_url"`
	FlairAllowlist   []string         `toml:"flair_allowlist"`
	FlairAllowlistRe []*regexp.Regexp `toml:"-"`
	FlairBlocklist   []string         `toml:"flair_blocklist"`
	FlairBlocklistRe []*regexp.Regexp `toml:"-"`
	TitleAllowlist   []string         `toml:"title_allowlist"`
	TitleAllowlistRe []*regexp.Regexp `toml:"-"`
	TitleBlocklist   []string         `toml:"title_blocklist"`
	TitleBlocklistRe []*regexp.Regexp `toml:"-"`
	TitleLimit       int              `toml:"title_limit"`
	BodyAllowlist    []string         `toml:"body_allowlist"`
	BodyAllowlistRe  []*regexp.Regexp `toml:"-"`
	BodyBlocklist    []string         `toml:"body_blocklist"`
	BodyBlocklistRe  []*regexp.Regexp `toml:"-"`
	BodyLimit        int              `toml:"body_limit"`
	Webhook          string           `toml:"webhook"`
}

func (r *RedditConfig) UserAgent() string {
	return fmt.Sprintf("%s/%s by /u/%s", r.AppName, r.Version, r.Username)
}

func (r *RedditConfig) SubRedditNames() []string {
	names := make([]string, len(r.SubReddits))
	for idx, sub := range r.SubReddits {
		names[idx] = sub.Name
	}
	return names
}
