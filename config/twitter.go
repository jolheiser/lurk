package config

type TwitterConfig struct {
	ConsumerKey    string `toml:"consumer_key"`
	ConsumerSecret string `toml:"consumer_secret"`
	AccessToken    string `toml:"access_token"`
	AccessSecret   string `toml:"access_secret"`

	Filters []Filter `toml:"filter"`
}

type Filter struct {
	Follows      []string `toml:"follows"`
	FollowStrict bool     `toml:"follow_strict"`
	Locations    []string `toml:"locations"`
	Tracks       []string `toml:"tracks"`
	Webhook      string   `toml:"webhook"`
}
